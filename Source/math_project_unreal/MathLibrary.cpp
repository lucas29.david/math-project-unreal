// Fill out your copyright notice in the Description page of Project Settings.


#include "MathLibrary.h"
#include "Math/UnrealMathUtility.h"


float UMathLibrary::RandomFloat(const float a, const float b) {
	return a + FMath::RandRange(0.0f, 1.0f) * (b - a);
}

int UMathLibrary::Bernoulli(const float p) {
	return RandomFloat() < p ? 1 : 0;
}

int UMathLibrary::BinomialDistribution (const int n, const float p) {
    int result = 0;
    for (int i = 0; i < n; i++) {
        result += Bernoulli(p);
    }
    return result;
}

