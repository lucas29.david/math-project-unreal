// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "MathLibrary.generated.h"

/**
 * 
 */
UCLASS()
class MATH_PROJECT_UNREAL_API UMathLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public: 
	UFUNCTION(BlueprintCallable, Category = "MathLibrary")
	static float RandomFloat(const float a = 0.0f, const float b = 1.0f);

	UFUNCTION(BlueprintCallable, Category = "MathLibrary")
	static int Bernoulli(const float p);

	UFUNCTION(BlueprintCallable, Category = "MathLibrary")
	static int BinomialDistribution(const int n, const float p);
};